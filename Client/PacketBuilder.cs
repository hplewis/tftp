/*
 * Holden Lewis
 */

using System;
using System.Text;
using System.Collections.Generic;

namespace Client {

    /**
     * Helper class for constructing packets to send to the remote server.
     * e.g. REQ, ACK, NACK
     */
    class PacketBuilder {

        private List<byte> packet;

        /**
         * Constructor a new packet builder.
         */
        public PacketBuilder() {
            this.packet = new List<byte>();
        }

        /**
         * Clear a packet builder.
         */
        public PacketBuilder Clear() {
            this.packet = new List<byte>();
            return this;
        }

        /**
         * Add a null byte. Usually used as a separator.
         */
        public PacketBuilder AddNullByte() {
            packet.Add(0x0);
            return this;
        }

        /**
         * Add a string to the packet.
         * @param s The string to add
         */
        public PacketBuilder AddString(string s) {
            packet.AddRange(Encoding.ASCII.GetBytes(s));
            return this;
        }

        /**
         * Add bytes to the packet.
         * @param bytes The bytes to add
         */
        public PacketBuilder AddBytes(byte[] bytes) {
            packet.AddRange(bytes);
            return this;
        }

        /**
         * Create a file request packet.
         * @param filename The file to request
         * @param withErrors Request with errors or not
         */
        public PacketBuilder CreateRequestPacket(string filename, bool withErrors) {
            Clear();

            AddBytes(withErrors ?
                    TftpConstants.REQUEST_WITH_ERROR : TftpConstants.REQUEST);

            AddString(filename);
            AddNullByte();
            AddString(TftpConstants.OCTET);
            AddNullByte();

            return this;
        }

        /**
         * Create an ack response packet.
         * @param block The block to ack
         */
        public PacketBuilder CreateAckPacket(byte[] block) {
            Clear();

            AddBytes(TftpConstants.ACK);
            AddBytes(block);

            return this;
        }

        /**
         * Create an nack response packet.
         * @param block The block to nack
         */
        public PacketBuilder CreateNackPacket(byte[] block) {
            Clear();

            AddBytes(TftpConstants.NACK);
            AddBytes(block);

            return this;
        }

        /**
         * Build the packet to send.
         */
        public byte[] Build() {
            return packet.ToArray();
        }

    }
}
