/*
 * Holden Lewis
 */

using System;

/**
 * Basic exception to signal a situation in which data cannot be recovered.
 */
public class UnrecoverableDataException: Exception {
    
    /**
     * New instance of the exception.
     */
    public UnrecoverableDataException() {
    }

    /**
     * New instance of the exception with a message.
     */
    public UnrecoverableDataException(string message) : base(message) {
    }

    /**
     * New instance of the exception with a message and
     * an encapsulated inner exception.
     */
    public UnrecoverableDataException(string message, Exception inner) : base(message, inner) {
    }
}
