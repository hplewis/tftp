/*
 * Holden Lewis
 */

using System;
using System.Collections.Generic;

namespace Client {

    /**
     * Parse server response packets.
     */
    class PacketParser {

        /**
         * Get the block number from a packet.
         * @param packet The packet to check
         */
        public static byte[] RetrieveBlockNumber(byte[] packet) {
            // TODO: validate packet opcode type
            
            // {0, 1, 2, 3, 4, 5} -> {2, 3}
            byte[] block = new byte[2];
            block[0] = packet[2];
            block[1] = packet[3];

            return block;
        }

        /**
         * Get opcode from packet.
         * @param packet packet to check
         */
        public static byte[] RetrieveOpcode(byte[] packet) {

            // {0, 1, 2, 3, 4, 5} -> {0, 1}
            byte[] opc = new byte[2];
            opc[0] = packet[0];
            opc[1] = packet[1];

            return opc;
        }

        /**
         * Get hex string for bytes.
         * @param packet Packet to convert
         * @param separator Between each byte
         */
        public static string BytesAsHex(byte[] packet, string separator) {
            string hex = "";
            foreach (byte b in packet) {
                hex += b.ToString("X2") + separator;
            }

            return hex == "" ? hex : hex.Substring(0, hex.Length - separator.Length);
        }

        /**
         * Get hex string for bytes.
         * @param packet Packet to convert
         */
        public static string BytesAsHex(byte[] packet) {
            return BytesAsHex(packet, " ");
        }

        /**
         * Get hex string for bytes.
         * @param bytes Bytes to convert
         */
        public static string BytesAsHex(List<byte> bytes) {
            return BytesAsHex(bytes.ToArray());
        }
    }
}
