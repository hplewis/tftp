/*
 * Holden Lewis
 */

using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Client {

    /**
     * A client class to handle a TFTP instance.
     */
    class TftpClient {

        private UdpClient client;
        private string hostname;
        private int port;
        private bool withErrors;

        /**
         * Instantiate a new TFTP client.
         * @param host The remote host name to connect to
         * @param port The remote port to connect to
         */
        public TftpClient(string host, int port) {
            // use default port locally
            this.client = new UdpClient(TftpConstants.TFTP_PORT);
            this.hostname = host;
            this.port = port;
            this.withErrors = false;

            this.client.Client.ReceiveTimeout = 5000;

            this.client.Connect(this.hostname, this.port);
        }

        /**
         * Close the UDP client.
         */
        public void Close() {
            this.client.Close();
        }

        /**
         * Request the file with errors in transmission (for testing).
         * @param withErrors boolean for whether to request with errors or not
         */
        public void RequestWithError(bool withErrors) {
            this.withErrors = withErrors;
        }

        /**
         * Send data to the remote server over UDP.
         * @param msg The byte array to send to the remote host
         */
        public void SendData(byte[] msg) {
            this.client.Send(msg, msg.Length);
        }

        /**
         * Receive data from remote server over UDP.
         * @param remote The IP endpoint of the remote host
         */
        public byte[] ReceiveData(IPEndPoint remote) {

            // block until data
            byte[] data = this.client.Receive(ref remote);
            // ^^ throws System.Net.Sockets.SocketException if timeout exceeded

            return data;
        }

        /**
         * Get a file from the remote host.
         * @param filename The name of the file to retrieve
         */
        public void GetFile(string filename) {

            Request(filename);

            IPEndPoint remote = new IPEndPoint(IPAddress.Any, 0);

            byte[] dataReceived;
            byte[] blockExpected = {0, 1};
            List<byte> parsedData = new List<byte>();
            bool nacked = false;
            do {
                nacked = false;
                dataReceived = ReceiveData(remote);
                byte[] opc = PacketParser.RetrieveOpcode(dataReceived);
                byte[] blockNumber = PacketParser.RetrieveBlockNumber(dataReceived);

                Logger.Log(String.Format("op[0x{0}] b[0x{1}]. Received {2} bytes.",
                        PacketParser.BytesAsHex(opc, ""),
                        PacketParser.BytesAsHex(blockNumber, ""),
                        dataReceived.Length));

                // check opcode
                if (!BlockEquals(opc, TftpConstants.DATA)) {
                    if (BlockEquals(opc, TftpConstants.ERROR)) {
                        string errorMessage = GetErrorFromResponse(dataReceived);
                        Console.WriteLine("Server Error: {0}", errorMessage);
                    } else {
                        Console.WriteLine("Invalid opcode response.");
                    }
                    return;
                }

                // Check block number to make sure we're in order
                if (!BlockEquals(blockNumber, blockExpected)) {
                    Logger.Log(String.Format("Unexpected block number. Expected block 0x{0}.",
                            PacketParser.BytesAsHex(blockExpected, "")));

                    // nack for block expected
                    Nack(blockExpected);
                    nacked = true;

                    // go around and read again
                    continue;
                }

                Hamming hammingBlock = new Hamming();
                bool errorFound = false;
                List<byte> processedBytes = new List<byte>();
                for (int i = 4; i < dataReceived.Length; i+=4) {
                    byte[] word = dataReceived.Skip(i).Take(4).ToArray();

                    try {
                        processedBytes.AddRange(hammingBlock.ProcessWord(word));
                    } catch (Exception) {
                        // nack & continue
                        errorFound = true;
                        break;
                    }
                }
                if (errorFound) {
                    Nack(blockExpected);
                    nacked = true;
                    continue;
                } else {
                    /* Logger.Log(String.Format("Processed Data: {{{0}}}", PacketParser.BytesAsHex(processedBytes.ToArray()))); */
                    parsedData.AddRange(processedBytes);
                    Ack(blockNumber);
                }

                IncrementBlock(blockExpected);
            } while (dataReceived.Length == 516 || nacked); // opcode 2. block 2. data 512.

            RemoveTrailingNullBytes(parsedData);

            // write data to file
            File.WriteAllBytes(filename, parsedData.ToArray());

        }

        /**
         * Remove trailing null bytes from a list of byte data.
         * @param data The bytes to check trailing null bytes for
         */
        private static void RemoveTrailingNullBytes(List<byte> data) {
            // find last non-zero character, add 1 to get first zero at end
            int i = data.FindLastIndex(x => x != 0) + 1;
            // remove everything from the last zero to the end
            data.RemoveRange(i, data.Count-i);
        }

        /**
         * Increment a block value.
         * @param block The block to increment
         */
        private void IncrementBlock(byte[] block) {
            // value0 value1
            // increment value1. if it wrapped around (now 0), then increment value0
            block[1]++;
            if (block[1] == 0) {
                block[0]++;
            }
        }

        /**
         * Check if two blocks are equal. Blocks are arrays of two bytes each.
         * @param block1 First block
         * @param block2 Second block
         */
        private bool BlockEquals(byte[] block1, byte[] block2) {
            return (block1[0] == block2[0]) && (block1[1] == block2[1]);
        }

        /**
         * Get the error message from a byte array response.
         * @param errorResponse The byte array response from the server
         */
        private string GetErrorFromResponse(byte[] errorResponse) {
            byte[] message = errorResponse.Skip(4).ToArray(); // remove opc and error num
            message = message.Take(message.Length - 1).ToArray(); // remove trailing null
            return Encoding.ASCII.GetString(message);
        }

        /**
         * Send a nack.
         * @param block The block number to nack
         */
        private void Nack(byte[] block) {
            byte[] nack = new PacketBuilder()
                .CreateNackPacket(block)
                .Build();
            Logger.Log(String.Format("Nacking block 0x{0}.", PacketParser.BytesAsHex(nack.Skip(2).Take(2).ToArray(), "")));
            SendData(nack);
        }

        /**
         * Send an ack.
         * @param block The block number to ack
         */
        private void Ack(byte[] block) {
            byte[] ack = new PacketBuilder()
                .CreateAckPacket(block)
                .Build();
            Logger.Log(String.Format("Acking block 0x{0}.", PacketParser.BytesAsHex(ack.Skip(2).Take(2).ToArray(), "")));
            SendData(ack);
        }

        /**
         * Send a file request packet.
         * @param filename The file to request
         */
        private void Request(string filename) {
            byte[] request = new PacketBuilder()
                .CreateRequestPacket(filename, this.withErrors)
                .Build();
            Logger.Log(String.Format("Requesting file {0}.", filename));
            SendData(request);
        }
    }
}
