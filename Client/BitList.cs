/*
 * Holden Lewis
 */

using System;
using System.Collections.Generic;

namespace Client {

    /**
     * A mutable list of bits with common operations for use with a Hamming code.
     */
    class BitList {

        private List<bool> bits;

        /**
         * New empty BitList.
         */
        public BitList() {
            bits = new List<bool>();
        }

        /*
         * New BitList with same data as another.
         * @param bitList other BitList to copy
         */
        public BitList(BitList bitList) {
            bits = new List<bool>();
            foreach (bool b in bitList.GetList()) {
                Add(b);
            }
        }

        /**
         * Make a BitList with specified bytes.
         * @param bytes Bytes to use
         */
        public BitList(byte[] bytes) {
            bits = new List<bool>();
            foreach (byte b in bytes) {
                string binaryOfByte = Convert.ToString(b,2);

                // pad beginning with zeros
                for (int i = 0; i < 8 - binaryOfByte.Length; i++) {
                    bits.Add(false);
                }
                foreach (char c in binaryOfByte) {
                    if (c == '1') {
                        bits.Add(true);
                    } else {
                        bits.Add(false);
                    }
                }
            }
        }

        /**
         * Number of bits in the BitList.
         */
        public int Count {
            get {
                return this.bits.Count;
            }
        }

        /**
         * If the BitList has a full byte to take.
         */
        public bool HasByte {
            get {
                return this.Count >= 8;
            }
        }

        /**
         * Reverse order of all bits in the BitList.
         */
        public void ReverseBits() {
            this.bits.Reverse();
        }

        /**
         * Reverse order of all bits within their respective byte boundaries.
         */
        public void SwapBitOrder() {
            List<bool> newBits = new List<bool>();
            int fullOctets = bits.Count / 8;
            int incompleteOctet = bits.Count % 8;

            for (int i = 0; i < fullOctets; i++) {
                for (int j = 7; j >= 0; j--) {
                    newBits.Add(bits[(i*8)+j]);
                }
            }
            for (int j = incompleteOctet - 1; j >= 0; j--) {
                newBits.Add(bits[(fullOctets*8)+j]);
            }
            this.bits = newBits;
        }

        /**
         * Get the list of bits.
         */
        public List<bool> GetList() {
            return this.bits;
        }

        /**
         * Add a bit to the BitList.
         * @param bit The bit to add
         */
        public void Add(bool bit) {
            this.bits.Add(bit);
        }
        
        /**
         * Add another BitList onto this one.
         * @param bitList Other BitList to add
         */
        public void AddBitList(BitList bitList) {
            foreach (bool b in bitList.GetList()) {
                Add(b);
            }
        }

        /**
         * Set the specified bit at an index.
         * @param bit The bit to set
         * @param index Where to set the bit
         */
        public void Set(bool bit, int index) {
            this.bits[index] = bit;
        }

        /**
         * Get a full byte from the BitList.
         * This removes the bits from the BitList.
         * This gets the byte from the first 8 bits in the BitList.
         * Precondition: HasByte must be true
         */
        public byte TakeByte() {
            byte resultByte = 0;
            for (int i = 0; i < 8; i++) {
                // shift left 1, then add bit
                resultByte <<= 1;
                if (this.bits[0]) {
                    resultByte += 1;
                }

                // remove head
                this.bits.RemoveAt(0);
            }
            
            return resultByte;
        }

        /**
         * Get the bit at an index.
         * @param index Which bit to get
         */
        public bool Get(int index) {
            return this.bits[index];
        }

        /**
         * Convert the BitList to a string.
         */
        public override string ToString() {
            string s = "";
            foreach (bool b in this.bits) {
                s += b ? "1" : "0";
            }
            return s;
        }

        /**
         * Convert the BitList to a string.
         * Also adds spacing options for readability.
         * @param spacingString The string to add between boundaries
         * @param spacingBoundary The bit boundary size
         */
        public string ToString(string spacingString, int spacingBoundary) {
            string s = "";
            for (int i = 0; i < this.bits.Count; i++) {
                s += this.bits[i] ? "1" : "0";
                if ( (i+1) % spacingBoundary == 0 ) {
                    s += spacingString;
                }
            }
            return s;
        }
    }
}
