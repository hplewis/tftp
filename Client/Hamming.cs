/*
 * Holden Lewis
 */

using System;
using System.Collections;
using System.Collections.Generic;

namespace Client {

    /**
     * Deal with hamming code data parsing, checking, removing, etc.
     */
    class Hamming {

        BitList processingQueue = new BitList();

        /**
         * Processing another word (4 bytes) of data).
         * @param word The word to process
         */
        public List<byte> ProcessWord(byte[] word) {

            if (word.Length != 4) {
                throw new ArgumentException("Word must be 4 bytes.");
            }

            Array.Reverse(word);
            BitList wordBits = new BitList(word);
            if (!CheckHammingBits(wordBits)) {
                throw new UnrecoverableDataException();
            }
            BitList dataBits = RemoveHammingBits(wordBits);
            processingQueue.SwapBitOrder(); // swap leftover bits
            processingQueue.AddBitList(dataBits);
            processingQueue.SwapBitOrder();

            List<byte> processedBytes = new List<byte>();
            while (processingQueue.HasByte) {
                byte frontByte = processingQueue.TakeByte();
                processedBytes.Add(frontByte);
            }

            return processedBytes;
        }

        /**
         * Check the hamming bits for data integrity. Fix issues if they arise.
         * @param wordBits The word (4 bytes) to check
         * @return bool True if no errors after correction. False if uncorrectable errors.
         */
        private bool CheckHammingBits(BitList wordBits) {
            wordBits.ReverseBits();

            int errorIndex = 0;
            // 1, 2, 4, 8, 16
            for (int i = 1; i < 32; i*=2) {
                bool even = true;
                // find each grouping
                for (int j = i; j < 32; j+=i*2) {
                    // get each element in the group
                    for (int g = 0; g < i; g++) {
                        if (wordBits.Get(j+g - 1)) {
                            even = !even;
                        }
                    }
                }

                if (!even) {
                    errorIndex += i;
                }
            }

            // hamming error
            if (errorIndex != 0) {
                bool wrongValue = wordBits.Get(errorIndex-1);
                wordBits.Set(!wrongValue, errorIndex-1);
            }

            // Check overall parity
            // 0..30 is the first 31 bits. 31 is the final parity
            bool parityOk = true;
            for (int i = 0; i < 32; i++) {
                if (wordBits.Get(i)) {
                    parityOk = !parityOk;
                }
            }

            wordBits.ReverseBits();
            return parityOk;
        }

        /**
         * Remove the hamming bits from a word.
         * @param wordBits The word to remove bits from
         */
        private BitList RemoveHammingBits(BitList wordBits) {
            BitList dataBits = new BitList();

            // go through each bit in the word
            for (int i = 0; i < 32; i++) {
                if (i != 0 && i != 16 && i != 24 && i != 28 && i != 30 && i != 31) {
                    dataBits.Add(wordBits.Get(i));
                }
            }
            return dataBits;
        }

    }
}
