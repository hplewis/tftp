/*
 * Holden Lewis
 */

namespace Client {

    /**
     * Store constant values for TFTP.
     */
    class TftpConstants {

        public const bool DEBUG = true;
        /* public const bool LOG_FILE = false; */

        public const int TFTP_PORT = 7000;

        public static readonly byte[] REQUEST = {0, 1};
        public static readonly byte[] REQUEST_WITH_ERROR = {0, 2};
        public static readonly byte[] DATA = {0, 3};
        public static readonly byte[] ACK = {0, 4};
        public static readonly byte[] ERROR = {0, 5};
        public static readonly byte[] NACK = {0, 6};

        public const string OCTET = "octet";
    }
}
