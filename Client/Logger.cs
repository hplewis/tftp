/*
 * Holden Lewis
 */

using System;
using System.IO;

namespace Client {

    /**
     * Control logging for the application.
     */
    class Logger {

        /**
         * Log a message.
         * @param message The message to log
         */
        public static void Log(string message) {
            if (TftpConstants.DEBUG) {
                Console.WriteLine(message);
            }
            /* if (TftpConstants.LOG_FILE) { */
            /*     File.AppendAllText("log", */
            /*             String.Format("[{0}]: {1}\n", DateTime.Now, message)); */
            /* } */
        }
    }
}
