﻿/*
 * Holden Lewis
 */

using System;
using System.Net.Sockets;

namespace Client {

    /**
     * Main class for the program. Contains main method.
     */
    class Program {

        /**
         * Main entrypoint for application
         * @param args Program arguments
         */
        static void Main(string[] args) {
            if (!AreArgumentsValid(args)) {
                Usage();
                return;
            }

            string host;
            string file;
            bool withErrors;

            if (args.Length == 2) {
                withErrors = false;
                host = args[0];
                file = args[1];
            } else {
                withErrors = args[0] == "error";
                host = args[1];
                file = args[2];
            }

            TftpClient client = new TftpClient(host, TftpConstants.TFTP_PORT);
            client.RequestWithError(withErrors);

            // attempt to get the file
            try {
                client.GetFile(file);
            } catch (SocketException) {
                Logger.Log("Timeout.");
            }

            client.Close();
        }

        /**
         * Print a usage message to the console.
         */
        private static void Usage() {
            Console.WriteLine("Usage: HammingTFTP.exe [error|noerror] tftp-host file");
        }

        /**
         * Check if arguments to the program are in a valid format.
         * @param args Program arguments to check
         */
        private static bool AreArgumentsValid(string[] args) {
            if (args.Length == 2) {
                return true;
            }
            if (args.Length == 3) {
                if (args[0] == "error" || args[0] == "noerror") {
                    return true;
                }
            }

            return false;
        }
    }
}
